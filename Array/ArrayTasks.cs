﻿using System;

namespace ArrayObject
{
    public class ArrayTasks
    {
        /// <summary>
        /// Task 1: Swap values of the first and last elements of the array, 
        /// the second and penultimate, etс.
        /// </summary>
        /// <param name="nums">An array of integers</param>
        /// <returns>Swaped array</returns>
        public static void ChangeElementsInArray(int[] nums)
        {
            int length = nums.Length;
            for (int i = 0; i < length / 2; i++)
            {
                if (nums[i] % 2 == 0 && nums[length - 1 - i] % 2 == 0)
                {
                    int elementInArray = nums[i];
                    nums[i] = nums[length - 1 - i];
                    nums[length - 1 - i] = elementInArray;
                }
            }
        }

        /// <summary>
        /// Task 2: Find distance between first and last occurrence of the max value in the array
        /// </summary>
        /// <param name="nums">An array of integers</param>
        /// <returns>Distance</returns>
        public static int DistanceBetweenFirstAndLastOccurrenceOfMaxValue(int[] nums)
        {
            int result = 0;
            if ((nums == null) || (nums.Length == 0))
                return result;

            int max = nums[0];
            int first = 0;
            int last = 0;
            for (int i = 1; i < nums.Length; i++)
            {
                int elementInArray = nums[i];
                if (elementInArray > max)
                {
                    max = elementInArray;
                    first = i;
                    last = i;
                }
                else if (elementInArray == max)
                {
                    last = i;
                }
            }
            result = last - first;
            return result;
        }

        /// <summary>
        /// Task 3: Change value to 0 in the elements below main diagonal and 1 - in elements above 
        /// </summary>
        /// <param name="matrix">Two-dimensional array (square matrix) of integers</param>
        /// <returns>Changed matrix</returns>
        public static void ChangeMatrixDiagonally(int[,] matrix)
        {
            int rows = matrix.GetUpperBound(0) + 1;
            int columns = matrix.Length / rows;
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    if (i > j)
                    {
                        matrix[i, j] = 0;
                    }
                    else if (i < j)
                    {
                        matrix[i, j] = 1;
                    }
                }
            }
        }
    }
    
}
